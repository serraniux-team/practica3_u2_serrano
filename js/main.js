




function CalcularT(){
    
    valor=document.getElementById("valor").value;
    if(valor==""){
        window.alert("Falto el valor del auto");
    }
    else{
        Plan=document.getElementById("tabla").value;
        
        valor=parseInt(valor);
        
        eng=enganche(valor);
        fin=financiar(valor,eng,Plan);
        pagm=pagomensual(fin,Plan);
        mostrar(eng,fin,pagm);
    }
    
    

    

}
//Funcion encargada de calcular el enganche 
function enganche(valort){
    
    return (30*valort)/100;
}
//Funcion encargada de calcular el total a financiar
function financiar(v,e,p){
    if(p==12){
        p=12.5;
    }
    else if(p==18){
        p=17.2;
    }
    else if(p==24){
        p=21;
    }
    else if(p==36){
        p=26;
    }
    else if(p==48){
        p=45;
    }
    
    var res=(v-e);
    var sum=(res*p)/100;
    totalf=((res+sum)).toFixed(2);
    
    return totalf;
}
//Funcion encargada de calcular el pago mensual
function pagomensual(tot,mes){
    
    return (tot/mes).toFixed(2) ;
}
//Funcion encargada de mostrar los resultados
function mostrar(eng,fin,pagm){
    
    
    document.getElementById('enganche').value="$"+eng;
    document.getElementById('total').value="$"+fin;
    document.getElementById('mensual').value="$"+pagm;

    
    

}
//Funcion encargada de limpiar los inputs
function Limpieza(){
    document.getElementById("valor").value="";
    document.getElementById('enganche').value="";
    document.getElementById('total').value="";
    document.getElementById('mensual').value="";
}
function Numeros(string){//Solo numeros
    var out = '';
    var filtro = '1234567890.';//Caracteres validos
	
    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
    for (var i=0; i<string.length; i++)
       if (filtro.indexOf(string.charAt(i)) != -1) 
             //Se añaden a la salida los caracteres validos
	     out += string.charAt(i);
	
    //Retornar valor filtrado
    return out;
} 